package com.jobo

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}

object Application extends App {
  val actorSystem = ActorSystem()
  implicit val executor = actorSystem.dispatcher
  val ref = actorSystem.actorOf(SimpleActor.prop)

  ref ! Hello
  ref ! Greetings("Andrew")
  ref ! GetVisitors
  ref ! Greetings("Ola")
  ref ! Greetings("Zoia")
  ref ! GetVisitors
}

class SimpleActor extends Actor with ActorLogging {

  override def receive: Receive = state()

  def state(storage: Seq[String] = Nil): Receive = {
    case Hello =>
      log.info("Hello Anonymous")
    case Greetings(name) =>
      log.info(s"`Greetings fom $name")
      val newState = state(storage :+ name)
      context.become(newState)
    case GetVisitors =>
      val visitors = storage.mkString(", ")
      log.info(visitors)
  }
}

object SimpleActor {
  def prop = Props(classOf[SimpleActor])
}

sealed trait Message
object Hello extends Message
object GetVisitors extends Message
case class Greetings(name: String) extends Message
